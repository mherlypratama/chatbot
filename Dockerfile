FROM node:16-alpine

WORKDIR /usr/src/app

COPY website/package*.json ./

RUN npm install

COPY website/ /usr/src/app/

EXPOSE 3000

CMD [ "node", "index.js" ]

# nanti coba pake RUN npm run start

